package com.artivisi.training.microservice201806.wallet.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity @Data
public class Wallet {

    @Id
    private String id;
    private String username;
    private BigDecimal balance;
}
