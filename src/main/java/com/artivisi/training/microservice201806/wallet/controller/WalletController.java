package com.artivisi.training.microservice201806.wallet.controller;

import com.artivisi.training.microservice201806.wallet.dao.WalletDao;
import com.artivisi.training.microservice201806.wallet.entity.Wallet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController @RequestMapping("/wallet")
public class WalletController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WalletController.class);

    @Autowired private WalletDao walletDao;

    @GetMapping("/{username}")
    public Wallet findByUsername(@PathVariable String username, Authentication currentUser){
        LOGGER.info("Current user : {}", currentUser);

        JwtAuthenticationToken user = (JwtAuthenticationToken) currentUser;
        String name=user.getName();
        LOGGER.info("Name : {}", name);
        LOGGER.info("Username : {}", ((Jwt)user.getPrincipal()).getClaims().get("user_name"));

        return walletDao.findByUsername(username);
    }

    @GetMapping("/my")
    public Wallet myWallet(Authentication currentUser) {
        JwtAuthenticationToken user = (JwtAuthenticationToken) currentUser;
        String username = (String) ((Jwt)user.getPrincipal()).getClaims().get("user_name");
        return walletDao.findByUsername(username);
    }

    @GetMapping("/currentuser")
    public Authentication currentUser(Authentication authentication) {
        return authentication;
    }

}
