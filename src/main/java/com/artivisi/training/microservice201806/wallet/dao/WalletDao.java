package com.artivisi.training.microservice201806.wallet.dao;

import com.artivisi.training.microservice201806.wallet.entity.Wallet;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface WalletDao extends PagingAndSortingRepository<Wallet, String> {
    Wallet findByUsername(String username);
}
