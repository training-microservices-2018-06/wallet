create table wallet (
  id varchar(255),
  username varchar(100) not null,
  balance numeric (19,2) not null,
  primary key (id),
  unique (username)
);